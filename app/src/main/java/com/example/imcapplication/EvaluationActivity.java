package com.example.imcapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

public class EvaluationActivity extends AppCompatActivity {

    private Button btnMain,btnEvaluation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation);

        btnMain = findViewById(R.id.activity_evaluation_btn_main);
        btnEvaluation = findViewById(R.id.activity_evaluation_btn_evaluation);

        btnEvaluation.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Ir a evaluaciones", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(view.getContext(), DetailActivity.class);
            startActivity(i);
            finish();
        });

        btnMain.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            finish();
        });
    }
}