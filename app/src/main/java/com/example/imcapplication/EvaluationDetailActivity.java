package com.example.imcapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.imcapplication.controllers.EvaluationController;
import com.example.imcapplication.models.Evaluation;

public class EvaluationDetailActivity extends AppCompatActivity {

    private TextView tvTitleId, tvTitle, tvDescription, tvDate;
    private Button btnDelete, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_detail);

        Evaluation evaluation = (Evaluation) getIntent().getSerializableExtra("evaluation");

        tvTitleId = findViewById(R.id.activity_evaluation_detail_tv_title_id);
        tvTitle = findViewById(R.id.activity_evaluation_detail_tv_title);
        tvDescription = findViewById(R.id.activity_evaluation_detail_tv_description);
        tvDate = findViewById(R.id.activity_evaluation_detail_tv_date);
        btnBack = findViewById(R.id.activity_evaluation_detail_btn_back);
        btnDelete = findViewById(R.id.activity_evaluation_detail_btn_delete);

        tvTitleId.setText(String.format("Detalles de la evaluacion %d", evaluation.getId()));
        tvTitle.setText(evaluation.getTitle());
        tvDescription.setText(evaluation.getDescription());
        tvDate.setText(evaluation.getStringDue());

        btnDelete.setOnClickListener(view -> {
            EvaluationController controller = new EvaluationController(view.getContext());
            controller.delete(evaluation.getId());
        });

        btnBack.setOnClickListener(view -> {
            super.onBackPressed();
        });
    }
}