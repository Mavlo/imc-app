package com.example.imcapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.example.imcapplication.models.Task;
import com.example.imcapplication.ui.TaskAdapter;

public class MainActivity extends AppCompatActivity {
    private ListView lvAllTasks;
    private Button btnLogout,btnEvaluation;

    private List<Task> taskList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvAllTasks = findViewById(R.id.activity_main_lv_all_tasks);
        btnLogout = findViewById(R.id.activity_main_btn_logout);
        btnEvaluation = findViewById(R.id.activity_main_btn_evaluation);


        for (int x = 0; x < 10; ++x) {
            Task newTask = new Task(String.format("Title %d", x), String.format("Description %d", x));
            newTask.setId(x);
            taskList.add(newTask);
        }

        TaskAdapter adapter = new TaskAdapter(this, taskList);

        lvAllTasks.setAdapter(adapter);

        btnLogout.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Cerrando Sesión", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(view.getContext(), LoginActivity.class);
            startActivity(i);
            finish();
        });

        btnEvaluation.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Ir a evaluaciones", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(view.getContext(), EvaluationActivity.class);
            startActivity(i);
            finish();
        });


    }
}