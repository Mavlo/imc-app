package com.example.imcapplication.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.example.imcapplication.dao.UserDao;
import com.example.imcapplication.models.UserEntity;
import com.example.imcapplication.utils.Converters;

@Database(entities = {UserEntity.class}, version = 1)
@TypeConverters({Converters.class})

public abstract class ImcApplicationDatabase extends RoomDatabase {

    private static final String DB_NAME = "imc_app_db";
    private static ImcApplicationDatabase instance;

    public static synchronized ImcApplicationDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), ImcApplicationDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();}
        return instance;
    }

    public abstract UserDao userDao();
}
