package com.example.imcapplication.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.imcapplication.dao.EvaluationDao;
import com.example.imcapplication.dao.UserDao;
import com.example.imcapplication.models.EvaluationEntity;
import com.example.imcapplication.models.UserEntity;
import com.example.imcapplication.utils.Converters;

@Database(entities = {UserEntity.class, EvaluationEntity.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class ImcAppDatabase extends RoomDatabase {
    private static final String DB_NAME = "imc_app_db";
    private static ImcAppDatabase instance;

    public static synchronized ImcAppDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), ImcAppDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    public abstract EvaluationDao evaluationDao();
}