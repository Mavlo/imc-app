package com.example.imcapplication.models;

import java.util.Date;

public interface IUser {

    long getId();

    String getFirstName();

    String getLastName();

    String getUserName();

    String getEmail();

    Date getBirthday();

    String getPassword();
}

