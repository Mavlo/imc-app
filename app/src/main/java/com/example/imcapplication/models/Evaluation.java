package com.example.imcapplication.models;

import java.io.Serializable;
import java.util.Date;

public class Evaluation implements Serializable {
    private long id;
    private Date date;
    private Double weight;

    public Evaluation(long id, Date date, Double weight) {
        this.id = id;
        this.date = date;
        this.weight = weight;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
