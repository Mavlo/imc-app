package com.example.imcapplication.models;

import java.util.Date;

public interface IEvaluation {
    long getId();
    String getTitle();
    String getDescription();
    Date getDate();
    long getUserId();
}
