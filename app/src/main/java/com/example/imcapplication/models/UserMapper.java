package com.example.imcapplication.models;

public class UserMapper {
    private IUser user;
    public UserMapper(IUser user) {
        this.user = user;
    }

    public UserEntity toEntity(){
        return new UserEntity(
                this.user.getId(),
                this.user.getFirstName(),
                this.user.getLastName(),
                this.user.getUserName(),
                this.user.getEmail(),
                this.user.getBirthday(),
                this.user.getPassword()
        );


    }




}
