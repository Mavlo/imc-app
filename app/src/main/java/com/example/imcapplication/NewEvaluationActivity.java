package com.example.imcapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcapplication.controllers.AuthController;
import com.example.imcapplication.controllers.EvaluationController;
import com.example.imcapplication.models.Evaluation;
import com.example.imcapplication.models.User;
import com.example.imcapplication.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewEvaluationActivity extends AppCompatActivity {

    private final String DATE_PATTERN = "yyyy-MM-dd";
    private TextInputLayout tilTitle, tilDescription, tilDate;
    private Button btnRegister, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_evaluation);

        tilTitle = findViewById(R.id.activity_new_evaluation_field_title);
        tilDescription = findViewById(R.id.activity_new_evaluation_field_description);
        tilDate = findViewById(R.id.activity_new_evaluation_field_date);
        btnRegister = findViewById(R.id.activity_new_evaluation_btn_register);
        btnBack = findViewById(R.id.activity_new_evaluation_btn_back);

        tilDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDate, new Date());
        });

        btnRegister.setOnClickListener(view -> {
            String title = tilTitle.getEditText().getText().toString();
            String description = tilDescription.getEditText().getText().toString();
            String date = tilDate.getEditText().getText().toString();

            // TODO: VALIDACIONES!

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

            Date dateDate = null;
            try {
                dateDate = dateFormatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            AuthController authController = new AuthController(view.getContext());

            User user = authController.getUserSession();

            Evaluation evaluation = new Evaluation(title, description, dateDate, user.getId());

            EvaluationController controller =new EvaluationController(view.getContext());

            controller.register(evaluation);

            Toast.makeText(view.getContext(), "Evaluacion registrada", Toast.LENGTH_SHORT).show();
        });

        btnBack.setOnClickListener(view -> {
            super.onBackPressed();
        });

    }
}